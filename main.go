package main

import (
  "fmt"
  "bufio"
  "os"
  "math/rand"
  "net/http"
  "net/url"
)

var targets = [1]string {"http://apple.icloud-wd.com/findIv8HB.html"}

func main() {
    fmt.Printf("Powering up....\n")

    f, err := os.Open("static/wordsEn.txt")
    if err != nil {
      fmt.Println(err)
      os.Exit(1);
    }
    defer f.Close()

    words := make([]string, 0)

    scanner := bufio.NewScanner(f);
    for scanner.Scan() {
      words = append(words, scanner.Text())
    }

    fmt.Println("How many times do you want to spam them?")
    count := 0
    fmt.Scanf("%d", &count)

    for i := 0; i < count; i++ {
        email := words[rand.Intn(len(words))] + "@" + words[rand.Intn(len(words))] + ".com"
        pass := words[rand.Intn(len(words))]
        send(email, pass)
    }

    if err := scanner.Err(); err != nil {
      fmt.Println(err)
    }
}

func send(email string, pass string) {
  fmt.Println(email, pass)
  target := targets[rand.Intn(len(targets))]

  resp, err := http.PostForm(target,
    url.Values{"email": {email}, "password": {pass}})

  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  fmt.Println("result: ", resp.Status)
}
